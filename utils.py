from urllib import urlopen
from json import loads
from google.appengine.api import memcache
import models
import datetime

def GetPagesNum(total, itemsPerPage):
    result = total % itemsPerPage
    if result == 0:
        return int(total / itemsPerPage)
    else:
        return int(total / itemsPerPage) + 1
    
def GetMatchesInfo(currentPage, pageSize, userID, apikey):
    http = urlopen("http://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/v1?"+
                   "key={0}&"
                   "account_id={1}&"
                   "matches_requested={2}".format(apikey, userID, pageSize))
    
    
def GetUserInfo(userID, apikey):
    userInfo = memcache.get(str(userID))
    if userInfo is None:
        http = urlopen("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={0}&format=json&steamids={1}".format(apikey, userID))
        json_object = loads(http.read())
        userInfo = json_object['response']['players'][0]
        memcache.add(str(userID), userInfo, 60 * 60)
    return userInfo

def GetUserTotalMatches(userID, apikey):
    userTotalMatches = memcache.get(str(userID) + "_matches")
    if userTotalMatches:
        return userTotalMatches
    else:
        http = urlopen("http://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/v1?key={0}&format=json&matches_requested=0&account_id={1}".format(apikey, userID))
        json_object = loads(http.read())
        memcache.add(str(userID) + "_matches", json_object['result']['total_results'], 60 * 60)
        return json_object['result']['total_results']
    
def getUser(vkID):
    return models.User.all().filter("vkID =", int(vkID)).get()