from google.appengine.ext import db

class User(db.Model):
    vkID = db.IntegerProperty(indexed = True)
    steamID = db.IntegerProperty(indexed = True)
    
class Match(db.Model):
    matchID = db.IntegerProperty(indexed = True)
    matchData = db.TextProperty()
    playersData = db.TextProperty()